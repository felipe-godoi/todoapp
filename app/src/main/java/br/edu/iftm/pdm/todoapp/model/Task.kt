package br.edu.iftm.pdm.todoapp.model

import android.os.Parcel
import android.os.Parcelable

class Task (
    val isUrgent: Boolean,
    val description: String,
    var isDone: Boolean = false,
    val id: Int? = 0,
){
}
