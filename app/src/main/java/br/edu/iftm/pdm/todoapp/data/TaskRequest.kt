package br.edu.iftm.pdm.todoapp.data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import br.edu.iftm.pdm.todoapp.model.Task
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

class TaskRequest(context: Context) {
    private val queue = Volley.newRequestQueue(context)
    companion object {
        private val URL = "http://10.0.2.2:8000"
        private val GET_TASKS = "/tasks"
        private val POST_TASKS = "/tasks/new"
        private val DELETE_TASK = "/tasks/del"
        private val UPDATE_TASK = "/tasks/done"
    }
    fun startTasksRequest() {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                tasksRequest()
                handler.postDelayed(this, 5000)
            }
        })
    }
    private fun tasksRequest() {
        var jsonRequest = JsonArrayRequest(
            Request.Method.GET,
            URL + GET_TASKS,
            null,
            { response ->
                val taskList = JSONArrayToTaskList(response)
                TasksSingleton.updateTaskList(taskList)
            },
            { error ->
                Log.e("TASKREQERR", "Task request error: ${error}")
            }
        )
        this.queue.add(jsonRequest)
    }
    fun JSONArrayToTaskList(jsonArray: JSONArray): ArrayList<Task> {
        val taskList = ArrayList<Task>()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObject = jsonArray.getJSONObject(i)
            val task = Task(
                jsonObject.getBoolean("is_urgent"),
                jsonObject.getString("description"),
                jsonObject.getBoolean("is_done"),
                jsonObject.getInt("id"),
            )
            taskList.add(task)
        }
        return taskList
    }

    fun deleteTask(task: Task) {
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.DELETE,
            URL + DELETE_TASK + '/' + task.id,
            this.taskToJSON(task),
            { response ->
                Log.d("RequestResponse", response.toString())
                this.tasksRequest()
            },
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }

    fun updateTask(task: Task, state: String) {
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.PUT,
            URL + UPDATE_TASK + '/' + state + '/' + task.id,
            this.taskToJSON(task),
            { response ->
                Log.d("RequestResponse", response.toString())
                this.tasksRequest()
            },
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }

    fun addTask(task: Task) {
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.POST,
            URL + POST_TASKS,
            this.taskToJSON(task),
            { response ->
                Log.d("RequestResponse", response.toString())
                this.tasksRequest()
            },
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }

    fun taskToJSON(task: Task): JSONObject {
        val jsonObject = JSONObject();
        jsonObject.put("description",task.description);
        jsonObject.put("is_done",task.isDone);
        jsonObject.put("is_urgent",task.isUrgent);

        return jsonObject;
    }

}