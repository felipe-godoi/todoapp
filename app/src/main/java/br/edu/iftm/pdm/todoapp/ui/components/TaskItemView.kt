package br.edu.iftm.pdm.todoapp.ui.components

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.edu.iftm.pdm.todoapp.R
import br.edu.iftm.pdm.todoapp.data.TaskRequest
import br.edu.iftm.pdm.todoapp.model.Task
import br.edu.iftm.pdm.todoapp.ui.theme.TodoAppTheme

@Composable
fun TaskItemView(task: Task, taskRequest: TaskRequest?) {
    var checked by remember {
        mutableStateOf(false)
    }
    checked = task.isDone
    var maxSize = remember { mutableStateOf(1)};
    var visibleState = remember { mutableStateOf(false) }


    Card(
        backgroundColor = MaterialTheme.colors.primary,
        modifier =
        Modifier
            .padding(horizontal = 5.dp, vertical = 2.5.dp)
            .fillMaxWidth()
    ){
        ShowDialog(visible = visibleState, task = task, taskRequest = taskRequest)
        Row (
            modifier = Modifier
                .height(intrinsicSize = IntrinsicSize.Min)
                .pointerInput(Unit) {
                    detectTapGestures(
                        onLongPress = {
                            visibleState.value = true;
                        },
                        onTap = {
                            if (maxSize.value == 1) maxSize.value = 1000 else maxSize.value = 1
                        }
                    )
                }){
            Box(modifier =
            Modifier
                .background(if (task.isUrgent) Color.Red else Color.Green)
                .width(20.dp)
                .fillMaxHeight()
            );
            Text(
                task.description,
                overflow = TextOverflow.Ellipsis,
                maxLines = maxSize.value,
                style = TextStyle(
                    textDecoration = if (task.isDone) TextDecoration.LineThrough else TextDecoration.None,
                    fontStyle = if (task.isDone) FontStyle.Italic else FontStyle.Normal,
                    fontWeight = if (task.isUrgent) FontWeight.Bold else FontWeight.Normal,
                    color = MaterialTheme.colors.onSecondary
                ),
                modifier = Modifier
                    .padding(start = 5.dp, end = 10.dp)
                    .fillMaxWidth()
                    .weight(1f)
                    .align(alignment = Alignment.CenterVertically)
            )
            Checkbox(
                checked = checked,
                onCheckedChange = {
                    checked = !checked
                    task.isDone = checked

                    if (checked) {
                        taskRequest?.updateTask(task, "t")
                    } else {
                        taskRequest?.updateTask(task, "f")
                    }
                },
                colors = CheckboxDefaults.colors(
                    checkedColor = MaterialTheme.colors.onSecondary,
                ),
                modifier = Modifier
                    .width(50.dp)
                    .align(alignment = Alignment.CenterVertically)
            );
            Text(
                stringResource(id = R.string.lbl_done),
                color = MaterialTheme.colors.onSecondary,
                modifier =
                Modifier
                    .align(Alignment.CenterVertically),
                style = TextStyle(
                    textDecoration = if (task.isDone) TextDecoration.LineThrough else TextDecoration.None,
                    fontStyle = if (task.isDone) FontStyle.Italic else FontStyle.Normal,

                    )
            )
        }
    }
}

@Composable
fun ShowDialog(visible: MutableState<Boolean>, task: Task, taskRequest: TaskRequest?)
{
    if(visible.value)
    {
        AlertDialog(
            onDismissRequest = { visible.value=false },
            dismissButton = {
                TextButton(onClick = { visible.value = false}) {
                    Text(text= "Cancel",
                        color = MaterialTheme.colors.onSecondary)
                }
            },
            confirmButton = {
                TextButton(onClick = {
                    visible.value = false
                    taskRequest?.deleteTask(task = task)
                }) {
                    Text(text="OK",
                        color = MaterialTheme.colors.onSecondary
                    )
                }
            },
            title = {Text(text="Excluir tarefa", color = MaterialTheme.colors.onSecondary)},
            text= { Text(text = "Tem certeza que deseja excluir a tarefa?", color = MaterialTheme.colors.onSecondary)}

        )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewTaskItemView() {
    val task = Task(false, "Tarefa 1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", true)
    TodoAppTheme(darkTheme = false) {
        TaskItemView(task, null)
    }
}