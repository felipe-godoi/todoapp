package br.edu.iftm.pdm.todoapp.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.ui.res.stringResource
import androidx.compose.material.*
import androidx.compose.material.TextFieldDefaults.outlinedTextFieldColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.edu.iftm.pdm.todoapp.data.TaskRequest
import br.edu.iftm.pdm.todoapp.R
import br.edu.iftm.pdm.todoapp.data.TasksSingleton
import br.edu.iftm.pdm.todoapp.model.Task
import br.edu.iftm.pdm.todoapp.ui.components.TaskItemView

@Composable
fun MainScreenView(taskRequest: TaskRequest) {
    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.secondary,
                modifier = Modifier.padding(bottom = 10.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.app_name),
                    fontSize = 20.sp,
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Left,
                    color = MaterialTheme.colors.onSecondary
                )
            }
        },
        bottomBar = {
            bottom(taskRequest)
        }
    ) { innerPadding ->
        taskList(innerPadding = innerPadding, taskRequest)
    }
}



@Composable
fun taskList(innerPadding: PaddingValues, taskRequest: TaskRequest) {

    Box(modifier = Modifier.padding(innerPadding)){
        Column() {
            LazyColumn() {
                items(TasksSingleton.getTasks()){ task ->
                    TaskItemView(task = task, taskRequest = taskRequest)
                }
            }
        }
    }
}
@Composable
fun bottom(taskRequest: TaskRequest) {
    val isUrgent = remember { mutableStateOf(false) }
    val taskName = remember { mutableStateOf(TextFieldValue("")) }
    Column {
        Row {
            Text(
                text = stringResource(id = R.string.lbl_urgent),
                textAlign = TextAlign.Left,
                modifier = Modifier.padding(start = 10.dp, end = 10.dp),
                fontSize = 20.sp,
                color = MaterialTheme.colors.onSecondary
            )
            Switch(
                checked = isUrgent.value,
                onCheckedChange = {
                    isUrgent.value = !isUrgent.value
                },
                colors = SwitchDefaults.colors(
                    uncheckedThumbColor = MaterialTheme.colors.secondary,
                    checkedThumbColor = MaterialTheme.colors.onSecondary
                )
            )
        }
        Row {
            OutlinedTextField(
                value = taskName.value,
                onValueChange = { value ->
                    taskName.value = value
                },
                colors = outlinedTextFieldColors(textColor = MaterialTheme.colors.onSecondary),
                placeholder = { Text(stringResource(id = R.string.input_message)) },
                modifier = Modifier
                    .padding(start = 10.dp, end = 10.dp, top = 5.dp, bottom = 5.dp)
                    .background(MaterialTheme.colors.primary)
                    .fillMaxWidth()
            )
        }
        Row {
            Button(
                onClick = {
                    taskRequest.addTask(Task(isUrgent = isUrgent.value, description = taskName.value.text, isDone = false))
                    taskName.value = TextFieldValue("")
                    isUrgent.value = false
                },
                modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp, end = 10.dp, bottom = 10.dp)
            ) { 
                Text(stringResource(id = R.string.btn_ok), color = MaterialTheme.colors.onSecondary)
            }
        }
    }

}

