package br.edu.iftm.pdm.todoapp.data

import androidx.compose.runtime.mutableStateListOf
import br.edu.iftm.pdm.todoapp.model.Task

object TasksSingleton {
    private val tasks = mutableStateListOf<Task>()
    fun updateTaskList(tasks: ArrayList<Task>) {
        this.tasks.clear()
        this.tasks.addAll(tasks)
    }
    fun getTasks(): List<Task> {
        return this.tasks
    }
}